<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
	public function listbarang(){
		$listjenis = DB::select ('SELECT COUNT(*) AS total, jenis_barang,
			(select count(*) from asset_wtmta aw where aw.jenis_barang=wtmta.jenis_barang and kondisi="hilang") as hilang,
			(select count(*) from asset_wtmta aw where aw.jenis_barang=wtmta.jenis_barang and kondisi="RUSAK") as rusak,
			(select count(*) from asset_wtmta aw where aw.jenis_barang=wtmta.jenis_barang and kondisi="BAIK") as baik,
			(select count(*) from asset_wtmta aw where aw.jenis_barang=wtmta.jenis_barang and kondisi="TERMINATE") as terminate, 
			(select count(*) from asset_wtmta aw where aw.jenis_barang=wtmta.jenis_barang and status="Used") as used, 
			(select count(*) from asset_wtmta aw where aw.jenis_barang=wtmta.jenis_barang and status="In TAG (Available)") as in_tag, 
			(select count(*) from asset_wtmta aw where aw.jenis_barang=wtmta.jenis_barang and status="Out TAG") as out_tag, 
			(select count(*) from asset_wtmta aw where aw.jenis_barang=wtmta.jenis_barang and status="Return / Available") as available 
			FROM asset_wtmta wtmta GROUP BY jenis_barang ORDER BY jenis_barang'
		);
		return view('user.dashboard', compact('listjenis'));
	}

	public function listcell($jenis_barang, $kondisi, $status){
		$jenis_barang = str_replace('*', '/', $jenis_barang);
		$assetjump = DB::table('asset_wtmta')
			->select('*', DB::raw('(select tgl_laporan from asset_laporan_teknisi where asset_wtmta.serial_number=asset_laporan_teknisi.sn limit 0,1) as last_laporan'));
		if($jenis_barang){
			$assetjump->where('jenis_barang', $jenis_barang);
			// echo"jb";
		}
		if($kondisi){
			$assetjump->where('kondisi', $kondisi);
			// echo"kn";
		}
		if($status){
			$assetjump->where('status',  str_replace('*', '/', $status));
		}
		$listasset = $assetjump->get();
		
		//dd($listasset);
		return view('user.list',compact('listasset'));
	}
}