<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
  public function index(){
  	//DB::select('select jenis_asset = asset_wmta')
		$data = DB::s('asset_jenis_lapora')->select('*', 'jenis_aset as text')->get();
		//dd($data);
		return view('setting', compact('data'));
	}

	public function inputjenis($id){
		$inputasset = DB::Table('asset_jenis_lapora')->where ('id'. $id)->first();
		return view('', compact('inputasset'));
	}
}