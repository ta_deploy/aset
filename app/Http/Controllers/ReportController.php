<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Illuminate\Support\Facades\Session;

class ReportController extends Controller
{
  public function report_1(){
    $last_laporan = DB::table('asset_laporan_master')->orderBy('id','desc')->first()->id;
    $asset = DB::table('asset_wtmta')->whereIn('jenis_barang', ['KBM FTTH','KBM STATION','KBM SKYWORKER','KBM PICK UP','OTDR','LAPTOP','PC','SPLICER', 'SEPEDA MOTOR'])->get();
		$teknisi = DB::select("SELECT COUNT(*) AS Rows, nik_pemakai, nama_pemakai FROM asset_wtmta where nik_pemakai!='' and jenis_barang IN('KBM FTTH','KBM STATION','KBM SKYWORKER','KBM PICK UP','OTDR','LAPTOP','PC','SPLICER', 'SEPEDA MOTOR') GROUP BY nik_pemakai ORDER BY Rows desc");
		$laporan = DB::select("SELECT * FROM asset_laporan_teknisi lt left join asset_laporan_master lm on lt.laporan_master_id = lm.id where lm.id = ?", [$last_laporan]);
		//dd($teknisi);
		return view('report.report', compact('asset', 'teknisi', 'laporan'));
	}
	public function history($sn){
		$data = DB::table('asset_laporan_teknisi')->where('sn', str_replace('*','/',$sn))->orderBy('id', 'desc')->get();
		// dd($data);
		return view('report.history', compact('data'));
	}
}