<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;

class ListController extends Controller
{
	public function list(){
		$listasset = DB::table('asset_wtmta')->get();
		//dd($datauser);
		return view('user.list',compact('listasset'));
	}

	public function input($id){
		$dataasset = DB::table('user')-> where ('id', $id)->first();
		return view('', compact('dataasset'));
	}
	public function search(Request $req){
		$listasset = DB::table('asset_wtmta')
			->select('*', DB::raw('(select tgl_laporan from asset_laporan_teknisi where asset_wtmta.serial_number=asset_laporan_teknisi.sn limit 0,1) as last_laporan'))
			->where('serial_number', 'like', '%'.$req->q.'%')
			->orWhere('nama_pemakai', 'like', '%'.$req->q.'%')
			->orWhere('jenis_barang', 'like', '%'.$req->q.'%')
			->get();
		return view('user.list',compact('listasset'));
	}
}
