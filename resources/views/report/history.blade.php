@extends('layout')
@section('css')
  <style type="text/css">
    
    img {
      width: 100px;
      height: 150px;
      margin-bottom: 5px;
    }
  </style>
@endsection
@section('heading')
    <h1>History Laporan Asset <u>{{ str_replace('*','/',Request::segment(2)) }}</u></h1>
@endsection
@section('content')
  <div class="widget-pricing widget-pricing-expanded">
    <div class="widget-pricing-inner row">
      @foreach($data as $no => $d)
        <div class="panel p-y-1 row">
          <div class="col-sm-3">
            Tanggal Laporan : {{ $d->tgl_laporan }}<br/>
            Status : <span class="label label-primary">{{ $d->status }}</span><br/>
            Keterangan : <code>{{ $d->keluhan }}</code>
            User : <code>{{ $d->user_nama }}</code>
          </div>
          <div class="col-sm-9">
            @foreach(scandir(public_path().'/storage/'.$d->id) as $index => $foto)
              @if($index > 1 && str_contains($foto, ['th']))
                <a href="{{ public_path().'/storage/'.$d->id }}">
                  <img src="{{ '/storage/'.$d->id.'/'.$foto }}" alt="" />
                </a>
              @endif
            @endforeach
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection
@section('js')
	<script type="text/javascript">
		$(function() {
    });
	</script>
@endsection